### QCore封装进度

```
QCore:

QAbstractAnimation        ░░░░░░░░░░         QAbstractEventDispatcher     ░░░░░░░░░░
QAbstractItemModel        ▓░░░░░░░░░         QAbstractListModel           ▓░░░░░░░░░
QAbstractNativeEventFilter░░░░░░░░░░         QAbstractProxyModel          ░░░░░░░░░░
QAbstractTableModel       ░░░░░░░░░░         QAdoptSharedDataTag          ░░░░░░░░░░
QAndroidActivityResultReceiver░░░░░░░░░░     QAndroidBinder               ░░░░░░░░░░
QAndroidIntent            ░░░░░░░░░░         QAndroidParcel               ░░░░░░░░░░
QAndroidService           ░░░░░░░░░░         QAndroidServiceConnection    ░░░░░░░░░░
QAnimationGroup           ░░░░░░░░░░         QAnyStringView               ░░░░░░░░░░
QAssociativeIterable      ░░░░░░░░░░         QAtomicInt                   ░░░░░░░░░░
QAtomicInteger            ░░░░░░░░░░         QAtomicPointer               ░░░░░░░░░░
QBEInteger                ░░░░░░░░░░         QBaseIterator                ░░░░░░░░░░
QBasicTimer               ░░░░░░░░░░         QBindable                    ░░░░░░░░░░
QBitArray                 ░░░░░░░░░░         QBuffer                      ░░░░░░░░░░
QByteArray                ░░░░░░░░░░         QByteArrayList               ░░░░░░░░░░
QByteArrayMatcher         ░░░░░░░░░░         QByteArrayView               ░░░░░░░░░░
QCache                    ░░░░░░░░░░         QCalendar                    ░░░░░░░░░░
QCborArray                ░░░░░░░░░░         QCborError                   ░░░░░░░░░░
QCborMap                  ░░░░░░░░░░         QCborParserError             ░░░░░░░░░░
QCborStreamReader         ░░░░░░░░░░         QCborStreamWriter            ░░░░░░░░░░
QCborValue                ░░░░░░░░░░         QChar                        ░░░░░░░░░░
QChildEvent               ░░░░░░░░░░         QCollator                    ░░░░░░░░░░
QCollatorSortKey          ░░░░░░░░░░         QCommandLineOption           ░░░░░░░░░░
QCommandLineParser        ░░░░░░░░░░         QConcatenateTablesProxyModel ░░░░░░░░░░
QConstIterator            ░░░░░░░░░░         QContiguousCache             ░░░░░░░░░░
QCoreApplication          ░░░░░░░░░░         QCryptographicHash           ░░░░░░░░░░
QDataStream               ░░░░░░░░░░         QDate                        ░░░░░░░░░░
QDateTime                 ░░░░░░░░░░         QDeadlineTimer               ░░░░░░░░░░
QDebug                    ░░░░░░░░░░         QDebugStateSaver             ░░░░░░░░░░
QDir                      ░░░░░░░░░░         QDirIterator                 ░░░░░░░░░░
QDynamicPropertyChangeEvent░░░░░░░░░░        QEasingCurve                 ░░░░░░░░░░
QElapsedTimer             ░░░░░░░░░░         QEnableSharedFromThis        ░░░░░░░░░░
QEvent                    ▓▓▓▓▓▓▓▓░░         QEventLoop                   ░░░░░░░░░░
QEventLoopLocker          ░░░░░░░░░░         QException                   ░░░░░░░░░░
QExplicitlySharedDataPointer░░░░░░░░░░       QFile                        ░░░░░░░░░░
QFileDevice               ░░░░░░░░░░         QFileInfo                    ░░░░░░░░░░
QFileSelector             ░░░░░░░░░░         QFileSystemWatcher           ░░░░░░░░░░
QFlag                     ░░░░░░░░░░         QFlags                       ░░░░░░░░░░
QFuture                   ░░░░░░░░░░         QFutureIterator              ░░░░░░░░░░
QFutureSynchronizer       ░░░░░░░░░░         QFutureWatcher               ░░░░░░░░░░
QGenericArgument          ░░░░░░░░░░         QGenericReturnArgument       ░░░░░░░░░░
QGlobalStatic             ░░░░░░░░░░         QGregorianCalendar           ░░░░░░░░░░
QHash                     ░░░░░░░░░░         QHashIterator                ░░░░░░░░░░
QHashSeed                 ░░░░░░░░░░         QIODevice                    ░░░░░░░░░░
QIODeviceBase             ░░░░░░░░░░         QIdentityProxyModel          ░░░░░░░░░░
QItemSelection            ░░░░░░░░░░         QItemSelectionModel          ▓░░░░░░░░░
QItemSelectionRange       ░░░░░░░░░░         QIterable                    ░░░░░░░░░░
QIterator                 ░░░░░░░░░░         QJalaliCalendar              ░░░░░░░░░░
QJniEnvironment           ░░░░░░░░░░         QJniObject                   ░░░░░░░░░░
QJsonArray                ░░░░░░░░░░         QJsonDocument                ░░░░░░░░░░
QJsonObject               ░░░░░░░░░░         QJsonParseError              ░░░░░░░░░░
QJsonValue                ░░░░░░░░░░         QJulianCalendar              ░░░░░░░░░░
QKeyCombination           ░░░░░░░░░░         QKeyValueIterator            ░░░░░░░░░░
QLEInteger                ░░░░░░░░░░         QLatin1Char                  ░░░░░░░░░░
QLatin1String             ░░░░░░░░░░         QLibrary                     ░░░░░░░░░░
QLibraryInfo              ░░░░░░░░░░         QLine                        ░░░░░░░░░░
QLineF                    ▓▓░░░░░░░░         QList                        ░░░░░░░░░░
QListIterator             ░░░░░░░░░░         QLocale                      ░░░░░░░░░░
QLockFile                 ░░░░░░░░░░         QLoggingCategory             ░░░░░░░░░░
QMap                      ░░░░░░░░░░         QMapIterator                 ░░░░░░░░░░
QMargins                  ░░░░░░░░░░         QMarginsF                    ░░░░░░░░░░
QMessageAuthenticationCode░░░░░░░░░░         QMessageLogContext           ░░░░░░░░░░
QMessageLogger            ░░░░░░░░░░         QMetaClassInfo               ░░░░░░░░░░
QMetaEnum                 ░░░░░░░░░░         QMetaMethod                  ░░░░░░░░░░
QMetaObject               ░░░░░░░░░░         QMetaProperty                ░░░░░░░░░░
QMetaSequence             ░░░░░░░░░░         QMetaType                    ░░░░░░░░░░
QMilankovicCalendar       ░░░░░░░░░░         QMimeData                    ░░░░░░░░░░
QMimeDatabase             ░░░░░░░░░░         QMimeType                    ░░░░░░░░░░
QModelIndex               ▓▓▓░░░░░░░         QModelRoleData               ░░░░░░░░░░
QModelRoleDataSpan        ░░░░░░░░░░         QMultiHash                   ░░░░░░░░░░
QMultiMap                 ░░░░░░░░░░         QMultiMapIterator            ░░░░░░░░░░
QMutableHashIterator      ░░░░░░░░░░         QMutableListIterator         ░░░░░░░░░░
QMutableMapIterator       ░░░░░░░░░░         QMutableMultiMapIterator     ░░░░░░░░░░
QMutableSetIterator       ░░░░░░░░░░         QMutex                       ░░░░░░░░░░
QMutexLocker              ░░░░░░░░░░         QObject                      ▓░░░░░░░░░
QObjectBindableProperty   ░░░░░░░░░░         QObjectCleanupHandler        ░░░░░░░░░░
QObjectComputedProperty   ░░░░░░░░░░         QOperatingSystemVersion      ░░░░░░░░░░
QParallelAnimationGroup   ░░░░░░░░░░         QPartialOrdering             ░░░░░░░░░░
QPauseAnimation           ░░░░░░░░░░         QPersistentModelIndex        ░░░░░░░░░░
QPluginLoader             ░░░░░░░░░░         QPoint                       ▓░░░░░░░░░
QPointF                   ▓▓▓▓▓▓▓▓░░         QPointer                     ░░░░░░░░░░
QProcess                  ░░░░░░░░░░         QProcessEnvironment          ░░░░░░░░░░
QPromise                  ░░░░░░░░░░         QProperty                    ░░░░░░░░░░
QPropertyAnimation        ░░░░░░░░░░         QPropertyBindingError        ░░░░░░░░░░
QPropertyChangeHandler    ░░░░░░░░░░         QPropertyData                ░░░░░░░░░░
QPropertyNotifier         ░░░░░░░░░░         QQueue                       ░░░░░░░░░░
QRandomGenerator          ░░░░░░░░░░         QRandomGenerator64           ░░░░░░░░░░
QReadLocker               ░░░░░░░░░░         QReadWriteLock               ░░░░░░░░░░
QRect                     ▓░░░░░░░░░         QRectF                       ▓░░░░░░░░░
QRecursiveMutex           ░░░░░░░░░░         QRegularExpression           ░░░░░░░░░░
QRegularExpressionMatch   ░░░░░░░░░░         QRegularExpressionMatchIterator░░░░░░░░░░
QResource                 ░░░░░░░░░░         QRomanCalendar               ░░░░░░░░░░
QRunnable                 ░░░░░░░░░░         QSaveFile                    ░░░░░░░░░░
QScopeGuard               ░░░░░░░░░░         QScopedArrayPointer          ░░░░░░░░░░
QScopedPointer            ░░░░░░░░░░         QScopedValueRollback         ░░░░░░░░░░
QSemaphore                ░░░░░░░░░░         QSemaphoreReleaser           ░░░░░░░░░░
QSequentialAnimationGroup ░░░░░░░░░░         QSequentialIterable          ░░░░░░░░░░
QSet                      ░░░░░░░░░░         QSetIterator                 ░░░░░░░░░░
QSettings                 ░░░░░░░░░░         QSharedData                  ░░░░░░░░░░
QSharedDataPointer        ░░░░░░░░░░         QSharedMemory                ░░░░░░░░░░
QSharedPointer            ░░░░░░░░░░         QSignalBlocker               ░░░░░░░░░░
QSignalMapper             ░░░░░░░░░░         QSize                        ▓▓░░░░░░░░
QSizeF                    ▓▓░░░░░░░░         QSocketNotifier              ░░░░░░░░░░
QSortFilterProxyModel     ░░░░░░░░░░         QStack                       ░░░░░░░░░░
QStandardPaths            ░░░░░░░░░░         QStaticByteArrayMatcher      ░░░░░░░░░░
QStaticPlugin             ░░░░░░░░░░         QStorageInfo                 ░░░░░░░░░░
QString                   ░░░░░░░░░░         QStringConverter             ░░░░░░░░░░
QStringDecoder            ░░░░░░░░░░         QStringEncoder               ░░░░░░░░░░
QStringList               ▓░░░░░░░░░         QStringListModel             ▓▓░░░░░░░░
QStringMatcher            ░░░░░░░░░░         QStringTokenizer             ░░░░░░░░░░
QStringView               ░░░░░░░░░░         QSysInfo                     ░░░░░░░░░░
QSystemSemaphore          ░░░░░░░░░░         QTaggedIterator              ░░░░░░░░░░
QTemporaryDir             ░░░░░░░░░░         QTemporaryFile               ░░░░░░░░░░
QTextBoundaryFinder       ░░░░░░░░░░         QTextStream                  ░░░░░░░░░░
QThread                   ░░░░░░░░░░         QThreadPool                  ░░░░░░░░░░
QThreadStorage            ░░░░░░░░░░         QTime                        ░░░░░░░░░░
QTimeLine                 ░░░░░░░░░░         QTimeZone                    ░░░░░░░░░░
QTimer                    ▓░░░░░░░░░         QTimerEvent                  ░░░░░░░░░░
QTranslator               ░░░░░░░░░░         QTransposeProxyModel         ░░░░░░░░░░
QTypeRevision             ░░░░░░░░░░         QUnhandledException          ░░░░░░░░░░
QUntypedBindable          ░░░░░░░░░░         QUrl                         ▓░░░░░░░░░
QUrlQuery                 ░░░░░░░░░░         QUtf8StringView              ░░░░░░░░░░
QUuid                     ░░░░░░░░░░         QVarLengthArray              ░░░░░░░░░░
QVariant                  ▓░░░░░░░░░░        QVariantAnimation            ░░░░░░░░░░
QVariantConstPointer      ░░░░░░░░░░         QVariantPointer              ░░░░░░░░░░
QVariantRef               ░░░░░░░░░░         QVector                      ░░░░░░░░░░
QVersionNumber            ░░░░░░░░░░         QWaitCondition               ░░░░░░░░░░
QWeakPointer              ░░░░░░░░░░         QWinEventNotifier            ░░░░░░░░░░
QWriteLocker              ░░░░░░░░░░         QXmlStreamAttribute          ░░░░░░░░░░
QXmlStreamAttributes      ░░░░░░░░░░         QXmlStreamEntityDeclaration  ░░░░░░░░░░
QXmlStreamEntityResolver  ░░░░░░░░░░         QXmlStreamNamespaceDeclaration░░░░░░░░░░
QXmlStreamNotationDeclaration░░░░░░░░░░      QXmlStreamReader             ░░░░░░░░░░
QXmlStreamWriter          ░░░░░░░░░░         QtFuture::WhenAnyResult      ░░░░░░░░░░

```