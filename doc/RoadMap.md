# 仓颉QT库需求汇总

## 1030版本特性清单：

| no   | issue   | feture description   | module   | owner    |
| :--- | ------------------------------------------------------------ | :----------------------------------------- | :----------------------- | :-------------------------------------- |
| 1    | [I5M6E6](https://gitee.com/HW-PLLab/qt/issues/I5M6E6) | 封装QTabBar组件  | Widgets | [@wathinst](https://gitee.com/wathinst) |
| 2    | [I5M6ED](https://gitee.com/HW-PLLab/qt/issues/I5M6ED) | 封装QTabWidget组件  | Widgets | [@wathinst](https://gitee.com/wathinst) |
| 3    | [I5M6CF](https://gitee.com/HW-PLLab/qt/issues/I5M6CF) | 封装QLineEdit组件  | Widgets | [@helin4576](https://gitee.com/helin4576) |
| 4    | [I5M6DM](https://gitee.com/HW-PLLab/qt/issues/I5M6DM) | 封装QScrollBar组件  | Widgets | [@helin4576](https://gitee.com/helin4576) |
| 5    | [I5M6D7](https://gitee.com/HW-PLLab/qt/issues/I5M6D7) | 封装QProgressBar组件  | Widgets |  |
| 6    | [I5M6EI](https://gitee.com/HW-PLLab/qt/issues/I5M6EI) | 封装QFrame组件  | Widgets |  |
| 7    | [I5M6EL](https://gitee.com/HW-PLLab/qt/issues/I5M6EL) | 封装QListView组件  | Widgets | [@helin4576](https://gitee.com/helin4576) |
| 8    | [I5M6EW](https://gitee.com/HW-PLLab/qt/issues/I5M6EW) | 封装QTableView组件  | Widgets |  |
| 9    | [I5M6F3](https://gitee.com/HW-PLLab/qt/issues/I5M6F3) | 封装QStackedWidget组件  | Widgets |  |
| 10   | [I5M6FC](https://gitee.com/HW-PLLab/qt/issues/I5M6FC) | 封装QSplitter组件  | Widgets | [@wathinst](https://gitee.com/wathinst) |
| 11   | [I5M6FJ](https://gitee.com/HW-PLLab/qt/issues/I5M6FJ) | 封装QSplitterHandle组件  | Widgets | [@wathinst](https://gitee.com/wathinst) |
| 12    |  | 封装QScrollArea组件  | Widgets | [@wathinst](https://gitee.com/wathinst) |