### QGui封装进度

```
QGui:

QAbstractFileIconProvider ░░░░░░░░░░         QAbstractTextDocumentLayout  ░░░░░░░░░░
QAccessible               ░░░░░░░░░░         QAccessibleActionInterface   ░░░░░░░░░░
QAccessibleEditableTextInterface░░░░░░░░░░   QAccessibleEvent             ░░░░░░░░░░
QAccessibleInterface      ░░░░░░░░░░         QAccessibleObject            ░░░░░░░░░░
QAccessiblePlugin         ░░░░░░░░░░         QAccessibleStateChangeEvent  ░░░░░░░░░░
QAccessibleTableCellInterface░░░░░░░░░░      QAccessibleTableInterface    ░░░░░░░░░░
QAccessibleTableModelChangeEvent░░░░░░░░░░   QAccessibleTextCursorEvent   ░░░░░░░░░░
QAccessibleTextInsertEvent░░░░░░░░░░         QAccessibleTextInterface     ░░░░░░░░░░
QAccessibleTextRemoveEvent░░░░░░░░░░         QAccessibleTextSelectionEvent░░░░░░░░░░
QAccessibleTextUpdateEvent░░░░░░░░░░         QAccessibleValueChangeEvent  ░░░░░░░░░░
QAccessibleValueInterface ░░░░░░░░░░         QActionGroup                 ▓▓░░░░░░░░
QBackingStore             ░░░░░░░░░░         QBitmap                      ░░░░░░░░░░
QBrush                    ▓▓▓▓▓▓░░░░         QClipboard                   ░░░░░░░░░░
QCloseEvent               ░░░░░░░░░░         QColor                       ▓▓▓▓▓▓░░░░
QColorSpace               ░░░░░░░░░░         QColorTransform              ░░░░░░░░░░
QConicalGradient          ░░░░░░░░░░         QContextMenuEvent            ▓░░░░░░░░░
QCursor                   ░░░░░░░░░░         QDesktopServices             ░░░░░░░░░░
QDoubleValidator          ▓▓▓▓▓▓▓▓▓▓         QDrag                        ░░░░░░░░░░
QDragEnterEvent           ░░░░░░░░░░         QDragLeaveEvent              ░░░░░░░░░░
QDragMoveEvent            ░░░░░░░░░░         QDropEvent                   ░░░░░░░░░░
QEnterEvent               ░░░░░░░░░░         QEventPoint                  ░░░░░░░░░░
QExposeEvent              ░░░░░░░░░░         QFileOpenEvent               ░░░░░░░░░░
QFileSystemModel          ░░░░░░░░░░         QFocusEvent                  ▓▓░░░░░░░░
QFont                     ▓░░░░░░░░░         QFontDatabase                ░░░░░░░░░░
QFontInfo                 ░░░░░░░░░░         QFontMetrics                 ░░░░░░░░░░
QFontMetricsF             ░░░░░░░░░░         QGenericMatrix               ░░░░░░░░░░
QGenericPlugin            ░░░░░░░░░░         QGenericPluginFactory        ░░░░░░░░░░
QGlyphRun                 ░░░░░░░░░░         QGradient                    ░░░░░░░░░░
QGuiApplication           ░░░░░░░░░░         QHelpEvent                   ░░░░░░░░░░
QHideEvent                ▓▓▓▓▓▓▓▓░░         QHoverEvent                  ░░░░░░░░░░
QIcon                     ▓▓▓░░░░░░░         QIconDragEvent               ░░░░░░░░░░
QIconEngine               ░░░░░░░░░░         QIconEnginePlugin            ░░░░░░░░░░
QImage                    ░░░░░░░░░░         QImageIOHandler              ░░░░░░░░░░
QImageIOPlugin            ░░░░░░░░░░         QImageReader                 ░░░░░░░░░░
QImageWriter              ░░░░░░░░░░         QInputDevice                 ░░░░░░░░░░
QInputEvent               ░░░░░░░░░░         QInputMethod                 ░░░░░░░░░░
QInputMethodEvent         ▓░░░░░░░░░         QInputMethodQueryEvent       ░░░░░░░░░░
QIntValidator             ░░░░░░░░░░         QKeyEvent                    ▓▓▓▓▓▓░░░░
QKeySequence              ░░░░░░░░░░         QMouseEvent                  ▓▓░░░░░░░░
QMoveEvent                ░░░░░░░░░░         QMovie                       ░░░░░░░░░░
QNativeGestureEvent       ░░░░░░░░░░         QOffscreenSurface            ░░░░░░░░░░
QOpenGLContext            ░░░░░░░░░░         QOpenGLContextGroup          ░░░░░░░░░░
QOpenGLExtraFunctions     ░░░░░░░░░░         QOpenGLFunctions             ░░░░░░░░░░
QOpenGLTexture            ░░░░░░░░░░         QPageLayout                  ░░░░░░░░░░
QPageRanges               ░░░░░░░░░░         QPageSize                    ░░░░░░░░░░
QPagedPaintDevice         ░░░░░░░░░░         QPaintDevice                 ▓░░░░░░░░░
QPaintDeviceWindow        ░░░░░░░░░░         QPaintEngine                 ░░░░░░░░░░
QPaintEngineState         ░░░░░░░░░░         QPaintEvent                  ▓▓▓▓▓▓░░░░
QPainter                  ▓▓▓▓▓▓░░░░         QPainterPath                 ░░░░░░░░░░
QPainterPathStroker       ░░░░░░░░░░         QPalette                     ▓▓▓▓▓▓░░░░
QPdfWriter                ░░░░░░░░░░         QPen                         ▓▓▓▓▓▓░░░░
QPicture                  ░░░░░░░░░░         QPixelFormat                 ░░░░░░░░░░
QPixmap                   ▓▓▓▓▓▓░░░░         QPixmapCache                 ░░░░░░░░░░
QPlatformSurfaceEvent     ░░░░░░░░░░         QPointerEvent                ░░░░░░░░░░
QPointingDevice           ░░░░░░░░░░         QPointingDeviceUniqueId      ░░░░░░░░░░
QPolygon                  ▓▓░░░░░░░░         QPolygonF                    ▓▓░░░░░░░░
QQuaternion               ░░░░░░░░░░         QRadialGradient              ░░░░░░░░░░
QRasterPaintEngine        ░░░░░░░░░░         QRasterWindow                ░░░░░░░░░░
QRawFont                  ░░░░░░░░░░         QRegion                      ░░░░░░░░░░
QRegularExpressionValidator░░░░░░░░░░        QResizeEvent                 ░░░░░░░░░░
QRgba64                   ░░░░░░░░░░         QRgbaFloat                   ░░░░░░░░░░
QScreen                   ░░░░░░░░░░         QScrollEvent                 ░░░░░░░░░░
QScrollPrepareEvent       ░░░░░░░░░░         QSessionManager              ░░░░░░░░░░
QShortcut                 ░░░░░░░░░░         QShortcutEvent               ░░░░░░░░░░
QShowEvent                ░░░░░░░░░░         QSinglePointEvent            ░░░░░░░░░░
QStandardItem             ▓░░░░░░░░░         QStandardItemModel           ▓▓▓▓░░░░░░
QStaticText               ░░░░░░░░░░         QStatusTipEvent              ░░░░░░░░░░
QStyleHints               ░░░░░░░░░░         QSupportedWritingSystems     ░░░░░░░░░░
QSurface                  ░░░░░░░░░░         QSurfaceFormat               ░░░░░░░░░░
QSyntaxHighlighter        ░░░░░░░░░░         QTabletEvent                 ░░░░░░░░░░
QTextBlock                ░░░░░░░░░░         QTextBlockFormat             ░░░░░░░░░░
QTextBlockGroup           ░░░░░░░░░░         QTextBlockUserData           ░░░░░░░░░░
QTextCharFormat           ░░░░░░░░░░         QTextCursor                  ░░░░░░░░░░
QTextDocument             ░░░░░░░░░░         QTextDocumentFragment        ░░░░░░░░░░
QTextDocumentWriter       ░░░░░░░░░░         QTextFormat                  ░░░░░░░░░░
QTextFragment             ░░░░░░░░░░         QTextFrame                   ░░░░░░░░░░
QTextFrameFormat          ░░░░░░░░░░         QTextImageFormat             ░░░░░░░░░░
QTextInlineObject         ░░░░░░░░░░         QTextItem                    ░░░░░░░░░░
QTextLayout               ░░░░░░░░░░         QTextLength                  ░░░░░░░░░░
QTextLine                 ░░░░░░░░░░         QTextList                    ░░░░░░░░░░
QTextListFormat           ░░░░░░░░░░         QTextObject                  ░░░░░░░░░░
QTextObjectInterface      ░░░░░░░░░░         QTextOption                  ░░░░░░░░░░
QTextTable                ░░░░░░░░░░         QTextTableCell               ░░░░░░░░░░
QTextTableCellFormat      ░░░░░░░░░░         QTextTableFormat             ░░░░░░░░░░
QTouchEvent               ░░░░░░░░░░         QTransform                   ▓░░░░░░░░░
QUndoCommand              ░░░░░░░░░░         QUndoGroup                   ░░░░░░░░░░
QUndoStack                ░░░░░░░░░░         QValidator                   ▓▓▓▓▓▓░░░░ 
QVector2D                 ░░░░░░░░░░         QVector3D                    ░░░░░░░░░░
QVector4D                 ░░░░░░░░░░         QVulkanDeviceFunctions       ░░░░░░░░░░
QVulkanExtension          ░░░░░░░░░░         QVulkanFunctions             ░░░░░░░░░░
QVulkanInfoVector         ░░░░░░░░░░         QVulkanInstance              ░░░░░░░░░░
QVulkanLayer              ░░░░░░░░░░         QVulkanWindow                ░░░░░░░░░░
QVulkanWindowRenderer     ░░░░░░░░░░         QWhatsThisClickedEvent       ░░░░░░░░░░
QWheelEvent               ▓░░░░░░░░░         QWindow                      ░░░░░░░░░░
QWindowStateChangeEvent   ░░░░░░░░░░         

```